# Tecnologías Web

## Universidad Nacional del Litoral

Proyecto de la materia 'Tecnologías Web' de la Universidad Nacional del Litoral. Creación del repositorio en GitLab, clonación del repositorio en la máquina local, modificación del archivo README.md, commit y push. Se suman los archivos HTML, CSS y JS de los proyectos anteriores.

<br>

## 🙋‍♂️ Hola, Soy Federico Krenn

:nerd_face: Desarrollador web Fullstack
<br>
👨‍🎓 Realizando la Tecnicatura en Desarrollo Web en ISPC y Tecnicatura en Software Libre en la UNL
<br>
📫 Conectemos en Linkedin: https://www.linkedin.com/in/fkrenn/
